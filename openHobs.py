#!/opt/epd/bin/python
#does observations on std-field in specified filter, dithered by dith-arcsec
#start is start time in UT
#SET: date/time, std-field, coords, filter (exposure)

from datetime import timedelta,datetime
from bgobs import readtargets

tag = 'LCOGT'
user = 'bj.fulton'
proposal = 'LCOELP-102'
priority = 40
inst = 'kb78'              #instrument/CCD
ag = ''
defocus = 0.0       #override this later if you wish
site = 'lsc'
domb = 'a'
ndays = 1

start = datetime( 2013, 4, 14, 4, 45, 0, 0)
end = datetime( 2013, 4, 14, 7, 45, 0, 0)
ctime = start

targets = readtargets('bgtargets_lsc.dat')


def makeObs(ra, dec, **vars):
	target_ra = ra
	target_de = dec

	from lcogtpond import block,molecule,pointing

	from datetime import datetime, timedelta
	block_params = {
	'start' : start,  #yyyy,mm,dd,hh,mm,ss,ms
	'end' :   ctime,
	'site' : site,
	'observatory' : 'dom'+domb,
	'telescope' : '1m0a',
	'priority' : priority
	}
	block_constraint_params = {
		'airmass' : 2.5,
		'trans' : 0.8,
		'seeing' : 5.0,
		'lunar_dist' : 36000,
		'lunar_phase' : 1.0
		}

	#block_params.update(block_constraint_params)
	block = block.Block.build(**block_params)

	if str(target_ra).find(':') != -1:
		ra_sex = [float(r) for r in target_ra.split(':')]
		ra_deg = ra_sex[0] + ra_sex[1]/60.0 + ra_sex[2]/3600.0
		ra_hr = ra_sex[0] + ra_sex[1]/60.0 + ra_sex[2]/3600.0
		ra_deg = ra_hr * 15.0
		dec_sex = [float(d) for d in target_de.split(':')]
		if dec_sex[0] > 0: dec_deg = dec_sex[0] + dec_sex[1]/60.0 + dec_sex[2]/3600.0
		else: dec_deg = dec_sex[0] - dec_sex[1]/60.0 - dec_sex[2]/3600.0
		
		target_ra = ra_deg
		target_de = dec_deg

	grid = '%s' % (name)
	ra = target_ra  #ignore cos(dec) for equatorial standards
	de = target_de
	point_params = {'ra':ra, 'dec':de}
	coords = pointing.ra_dec(**point_params)
	sid_target = pointing.sidereal(name=grid,coord=coords)

	expose_params = {
	'tag' : tag,
	'user' : user,
	'proposal' : proposal,
	'group' : name,
	'inst_name' : inst,
	'pointing' : sid_target,
	'ag_mode' : 0,
	'ag_name' : ag,
	'defocus' : defocus}
	expose_priority = 0  # get observations in order
	for j in range(rpt):
		for i in range(len(filt)):          
			print grid, ra, de, filt[i], bin[i], cnt[i]
			new_params = {'exp_cnt':cnt[i],'exp_time':exp[i], 'bin':bin[i], 'filters':filt[i],'priority':expose_priority}
			expose_params.update(new_params)
			expose = molecule.Expose.build(**expose_params)
			block.add_molecule(expose)
			expose_priority += 1

	block.save()


while ctime <= end:
	for target in targets:
		name = target['NAME']
		target_ra = target['RA']
		target_dec = target['DEC']
		filt = ['B','V','ip']   #filter, dashes
		exp = [300,300,300]
		bin  = [2,2,2]     #binning
		cnt = [1,1,1]
		rpt = 1     #number of repeats
		defocus = 0.0

		ctime += timedelta(seconds=945)
		makeObs(target_ra, target_dec, **locals())
		start += timedelta(seconds=945)
	
