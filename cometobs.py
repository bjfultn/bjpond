#!/opt/epd/bin/python
#does observations on std-field in specified filter, dithered by dith-arcsec
#start is start time in UT
#SET: date/time, std-field, coords, filter (exposure)

from datetime import timedelta,datetime
import astrolib as al

tag = 'LCOGT'
user = 'bj.fulton'
proposal = 'LCOELP-102'
priority = 30
inst = 'kb74'              #instrument/CCD
ag = ''
site = 'elp'
domb = 'a'
ndays = 1

start = datetime( 2013, 3, 15, 1, 50, 0, 0 )
end = datetime( 2013, 3, 15, 2, 05, 0, 0 )

epochofperih = al.date2jd(datetime(2013, 03, 10, 0, 0, 0) + timedelta(days=0.1696)) - 2400000.5

point_params = {
'name' : "C2011L4",
'epochofel' : 56200.5000000,
'orbinc' : 84.2072,
'longascnode' : 65.6658,
'argofperih' : 333.6517,
'perihdist' : 0.301542,
'eccentricity' : 1.000028,
'epochofperih' : epochofperih
}

#epochofperih = al.date2jd(datetime(2013, 11, 28, 0, 0, 0) + timedelta(days=0.7590)) - 2400000.5

#point_params = {
#'name' : "C/2012_S1",
#'epochofel' : 56200.5000000,
#'orbinc' : 62.1120,
#'longascnode' : 295.6949,
#'argofperih' : 345.5360,
#'perihdist' : 0.012471,
#'eccentricity' : 1.000005,
#'epochofperih' : epochofperih
#}

name = point_params['name']
exp = [5,5,5]
filt = ['B','V','ip']
bin = [2,2,2]
cnt = [1,1,1]
rpt = 15

print locals()

def makeObs(**vars):
	from lcogtpond import block,molecule,pointing

	from datetime import datetime, timedelta
	block_params = {
	'start' : start,  #yyyy,mm,dd,hh,mm,ss,ms
	'end' :   end,
	'site' : site,
	'observatory' : 'dom'+domb,
	'telescope' : '1m0a',
	'priority' : priority
	}
	block_constraint_params = {
		'airmass' : 2.5,
		'trans' : 0.8,
		'seeing' : 5.0,
		'lunar_dist' : 36000,
		'lunar_phase' : 1.0
		}

	#block_params.update(block_constraint_params)
	block = block.Block.build(**block_params)

	grid = '%s' % (name)
	#point_params = {'ra':ra, 'dec':de}
	#coords = pointing.ra_dec(**point_params)
	#sid_target = pointing.sidereal(name=grid,coord=coords)
	sid_target = pointing.nonsidereal_mpc_comet(**point_params)

	expose_params = {
	'tag' : tag,
	'user' : user,
	'proposal' : proposal,
	'group' : name,
	'inst_name' : inst,
	'pointing' : sid_target,
	'ag_mode' : 2,
	'ag_name' : ag}
	expose_priority = 0  # get observations in order
	for j in range(rpt):
		for i in range(len(filt)):          
			print grid, filt[i], bin[i], cnt[i]
			new_params = {'exp_cnt':cnt[i],'exp_time':exp[i], 'bin':bin[i], 'filters':filt[i],'priority':expose_priority}
			expose_params.update(new_params)
			expose = molecule.Expose.build(**expose_params)
			block.add_molecule(expose)
			expose_priority += 1

	block.save()


for i in range(ndays):
	makeObs(**locals())
	start += timedelta(days=1)
	end += timedelta(days=1)
