#!/usr/bin/env python

from datetime import datetime,timedelta
import numpy as np
import sys
import argparse
from rise_set.astrometry import calc_sunrise_set,Angle
from glob import glob
from mypond import submitObs,findFreeTime
from astrolib import date2jd,hourAngle
import argparse

sqa = {'latitude':Angle(degrees=34.687604),'longitude':Angle(degrees=-120.039067), 'name':'sqa'}
bpl = {'latitude':Angle(degrees=34.4325),'longitude':Angle(degrees=-119.863056), 'name':'bpl'}
elp = {'latitude':Angle(degrees=30.67),'longitude':Angle(degrees=-104.02),'name':'elp'}

def earlyTarget(targetfile,length=timedelta(hours=0.5),halim=5.25):
    free = findFreeTime(cal_user,cal_passwd,sdate,debug=False,site=sqa)
    if len(free) < 1: return False

    planfile = open(targetfile,'r')
    content = planfile.read()
    target = {}
    target['RA'] = content.split('\n')[-2].split()[-2]
    target['DEC'] = content.split('\n')[-2].split()[-1]
    target['FILTER'] = ''
    target['EXPTIME'] = 0.
    target['BINNING'] = 2
    target['OBJECT'] = target['NAME'] = ''.join(content.split('\n')[-2].split()[0:-2])
    block = free[0]
    start = block[0]
    HA = hourAngle(target,start)
    end = start+length
    block = (start,end)
    if abs(HA) < halim and (end < free[0][1]):
        print "\nInserting %s" % target['OBJECT'], "%s --> %s" % block
        print content
        if not opt.dry:
            print "Submitting %s observations to Goggle Calendar..." % (target['OBJECT'])
            submitObs(cal_user,cal_passwd,target,block,content,location=None,site=site['name'])
        return True
    else:
        print "No time for %s." % target['OBJECT']
        return False

def lateTarget(targetfile,length=timedelta(hours=0.5),halim=5.25):
    free = findFreeTime(cal_user,cal_passwd,sdate,debug=False,site=sqa)
    if len(free) < 1: return False
    
    planfile = open(targetfile,'r')
    content = planfile.read()
    target = {}
    target['RA'] = content.split('\n')[-2].split()[-2]
    target['DEC'] = content.split('\n')[-2].split()[-1]
    target['OBJECT'] = target['NAME'] = ''.join(content.split('\n')[-2].split()[0:-2])
    target['FILTER'] = ''
    target['EXPTIME'] = 0.
    target['BINNING'] = 2

    block = free[-1]
    start = block[1] - length
    end = block[1]
    block = (start,end)
    HA = hourAngle(target,start)
    if abs(HA) < halim and (start > free[-1][0]):
        print "\nInserting %s" % target['OBJECT'], "%s --> %s" % block
        print content
        if not opt.dry:
            print "Submitting %s observations to Goggle Calendar..." % (target['OBJECT'])
            submitObs(target,block,user,location=None,site=site['name'])
        return True
    else:
        print "No time for %s." % target['OBJECT']
        return False

def main():
    #GKTau is first
    #earlyTarget('monitor_targets/GKTau.acp',halim=2.5)

    #fred008 (the shrimp) is next target
    #earlyTarget('monitor_targets/shrimp.acp',halim=2.5)
    
    #SN2012da
    t = True
    for i in range(2):
        if t: t = earlyTarget('/home/bfulton/code/sqacontrol/monitor_targets/SN2012da.acp',halim=4.5,length=timedelta(minutes=17))

    #Add 1 PTF11kly block 
    t = True
    for i in range(1):
        if t: t = earlyTarget('/home/bfulton/code/sqacontrol/monitor_targets/PTF11kly.acp',length=timedelta(minutes=34),halim=3.5)
    
    #Add up to 4 NGC6946 color blocks
    #t = True
    #for i in range(5):
    #    if t: t = lateTarget('/home/bfulton/code/sqacontrol/monitor_targets/ngc6946.acp',length=timedelta(minutes=28),halim=3.5)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Enter observations into the BOS Google Cal')
    parser.add_argument('-d',dest='sdate',action='store',help='UT date of intented observations (YYYYMMDD)',default=datetime.strftime(datetime.today()+timedelta(days=1), format='%Y%m%d'),type=str)
    parser.add_argument('-n',dest='dry',action='store_true',help='Dry run. Print results, but do not upload to Google Cal.',default=False)
    parser.add_argument('-f',dest='file',action='store',help='File containing target.',type=str)
    parser.add_argument('-o',dest='owner',action='store',help='Owner of events.',default='bj.fulton',type=str)
    parser.add_argument('-p',dest='proposal',action='store',help='Proposal ID',default='LCOELP-102',type=str)
    parser.add_argument('--site',dest='site',action='store',help='Site',default='elp',type=str)
    parser.add_argument('--tel',dest='tel',action='store',help='Telescope',default='1m0a')

    global opt
    opt = parser.parse_args()

    targetfile = opt.file

    sdate = datetime.strptime(opt.sdate,'%Y%m%d')


    main()
