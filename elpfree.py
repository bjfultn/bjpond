#!/usr/bin/env python

from rise_set.astrometry import calc_sunrise_set,Angle
from datetime import datetime,timedelta
import numpy as np
import sys
from mypond import getEvents,findFreeTime

elp = {'latitude':Angle(degrees=30.67),'longitude':Angle(degrees=-104.02),'name':'elp'}

fromaddr = 'ELP <bjfulton@lcogt.net>'
toaddr = [
'bjfulton@lcogt.net',
'tlister@lcogt.net',
'tbrown@lcogt.net',
'rstreet@lcogt.net',
'mgraham@lcogt.net',
'ashporer@lcogt.net',
'jeastman@lcogt.net',
'wrosing@lcogt.net',
'fbianco@lcogt.net',
'ahowell@lcogt.net'
]
#toaddr = (',').join(toaddr)

subject = '[ELP Schedule]'
usr = 'bjfulton@lcogt.net'
passwd = '87138104'

def sendMsg(msg):
    import smtplib
    import urllib2
    
    head = """From: %s
To: %s
Subject: %s

""" % (fromaddr,toaddr,subject)

    msg = head + msg + "**All dates/times are shown in UTC"

    try:
        server = smtplib.SMTP('smtp.gmail.com',587)
        server.ehlo()
        server.starttls()
        server.ehlo
        server.login(usr,passwd)
        server.sendmail(fromaddr,toaddr,msg)
        server.quit()
        stat = 0
        sent = datetime.now()
    except:
        stat = -1
        sent = datetime.now()
        print "Message did not send."
    
    return stat,sent

if __name__ == '__main__':
    
    import argparse

    parser = argparse.ArgumentParser(description='Find free time for a specific night on the ELP 1m0-08 telescope.',add_help=True)
    parser.add_argument('-d',dest='sdate',action='store',help='UT date at start of night to search (YYYYMMDD)',default=datetime.strftime(datetime.utcnow()+timedelta(days=1), format='%Y%m%d'),type=str)
    parser.add_argument('-e',dest='email',action='store_true',help='Send notification e-mail',default=False)
    #parser.add_argument('--tel',dest='tel',action='store',help='Telescope',default='1m0a')
    #parser.add_argument('--site',dest='site',action='store',help='Site',default=elp)
    opt = parser.parse_args()

    sdate = datetime.strptime(opt.sdate,'%Y%m%d')
    if opt.email:
        oldout = sys.stdout
        msgfile = open('msg.txt','w')
        sys.stdout = msgfile
        free = findFreeTime(sdate=sdate,site=elp)
        msgfile.close()
        msgfile = open('msg.txt','r')
        msg = msgfile.read()
        sys.stdout = oldout
        print msg
        stat,sent = sendMsg(msg)
    else:
        free = findFreeTime(sdate=sdate,site=elp)
