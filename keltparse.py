#!/usr/bin/env python

import urllib2, base64
import argparse
from datetime import datetime, timedelta
from sites import *
import astrolib as al
import numpy as np

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parse and schedule KELT obs into POND')
    parser.add_argument('-d',dest='sdate',action='store',help='UT date of intented observations (YYYYMMDD)',default=datetime.strftime(datetime.today()+timedelta(days=1), format='%Y%m%d'),type=str)
    parser.add_argument('-s',dest='days',action='store',help='Search N days from start date.',default=1,type=int) 
    parser.add_argument('-n',dest='dry',action='store_true',help='Dry run. Print results, but do not upload to Google Cal.',default=False)
    parser.add_argument('-a',dest='all',action='store_true',help='Schedule all events regardless of existing blocks.',default=False)
    parser.add_argument('--inter',dest='interactive',action='store_true',help='Interactive?.',default=False)
    parser.add_argument('-f',dest='filter',action='store',help='Filter',default='ip',type=str)
    parser.add_argument('-o',dest='owner',action='store',help='Owner of events.',default='bj.fulton',type=str)
    parser.add_argument('-p',dest='proposal',action='store',help='Proposal ID',default='LCOELP-102',type=str)
    parser.add_argument('--priority',dest='priority',action='store',help='priority', default=40,type=int)
    parser.add_argument('--site',dest='site',action='store',help='Site',default='elp',type=str)
    parser.add_argument('--tel',dest='tel',action='store',help='Telescope',default='1m0a')
    parser.add_argument('--enc',dest='enc',action='store',help='Observatory',default='doma')
    parser.add_argument('--pad',dest='pad',action='store',help='Length of OoT observations on either side of transit. (hours)',default=1.33,type=float)
    parser.add_argument('--twilight',dest='twilight',action='store',help='Twilight (sunset|civil|nautical|astronomical)',default='civil')
    


    global opt
    opt = parser.parse_args()
    owner = opt.owner
    proposal = opt.proposal
    priority = 51

    halim = 5.0

    sdate = datetime.strptime(opt.sdate,'%Y%m%d')
    sdate_str = datetime.strftime(sdate, '%m-%d-%Y')

    site = eval(opt.site)
    opt.site = site
    opt.obs = opt.enc
    if site['longitude'].degrees > 0: sdate -= timedelta(days=1)

    url = 'http://astro.swarthmore.edu/telescope/kelt/print_eclipses.cgi?observatory_string=%s&use_utc=1&observatory_latitude=%5.3f&observatory_longitude=%5.3f&timezone=UTC&start_date=%s&days_to_print=%d&days_in_past=1&minimum_start_elevation=30+&and_vs_or=or&minimum_end_elevation=30+&minimum_priority=2&minimum_depth=0&target_string=&single_object=0&target=&ra=&dec=&epoch=&period=&duration=&show_ephemeris=0&print_html=2' % (site['obstr'],site['latitude'].degrees,site['longitude'].degrees, sdate_str, opt.days)

    print url
    request = urllib2.Request(url)
    base64string = base64.encodestring('%s:%s' % ('kelt_observer', 'NoEBs!')).replace('\n', '')
    request.add_header("Authorization", "Basic %s" % base64string)   
    response = urllib2.urlopen(request)
    data = response.read()

    data = data[data.find('<pre>')+5:data.find('</pre>')]

    print data

    keltpriority = []
    lines = []
    for line in data.split('\n'):
        if not line.startswith('K'): continue
        keltpriority.append(long(line.split(', ')[17]))
        lines.append(line)
    order = np.argsort(keltpriority)[::-1]
    lines = np.array(lines)[order]
    
    for line in lines:
        if not line.startswith('K'): continue
        target = {}
        lsplit = line.split(', ')
        target['NAME'] = target['name'] = lsplit[0]
        coords = lsplit[15].split(' ')
        target['ra'] = target['RA'] = coords[0]
        target['dec'] = target['DEC'] = coords[1]

        target['filter'] = target['FILTER'] = opt.filter
        target['binning'] = target['BINNING'] = 2
        target['owner'] = owner
        target['proposal'] = proposal
        target['priority'] = priority
        target['defocus'] = 0.0

        m0 = 12.0
        if opt.filter == 'zs':
            m0 = 10.5
            target['defocus'] = 2.0
        if site == sqa: m0 -= 0.5
        mag = float(lsplit[1])
        deltamag = mag - m0
        exp = 40.0 * 10 ** (deltamag/2.5)

        target['exp'] = target['EXPTIME'] = round(exp)
        while exp < 30:
            target['defocus'] += 1.5
            target['exp'] = target['EXPTIME'] = exp = exp + 10.0

        start_dt = datetime.strptime(lsplit[2], '%m-%d-%Y %H:%M')
        #if start_dt - datetime.utcnow() > timedelta(days=1+opt.days):
            #print "Block too far in advance"
            #continue
        
        end_dt = datetime.strptime(lsplit[4], '%m-%d-%Y %H:%M')
        if end_dt < datetime.utcnow():
            print "End of block in past"
            continue

        opt.sdate = start_dt
        dur = end_dt - start_dt
        target['duration'] = dur.seconds / 3600.
        #ha_start = float(lsplit[12])
        #ha_end = float(lsplit[14])

        start_dt -= timedelta(hours=opt.pad)
        end_dt += timedelta(hours=opt.pad)
        ha_start = al.hourAngle(target,start_dt,site)
        ha_end = al.hourAngle(target,end_dt,site)
        if ha_end < ha_start: ha_end += 12.

        if ha_start < -halim:
            start_dt -= timedelta(hours=(ha_start + halim))
            print "HAlimit: Adjusting start time to:", start_dt, (ha_start+halim)
            ha_start = -halim
            block = (start_dt,end_dt)
        elif ha_end > halim:
            end_dt -= timedelta(hours=np.abs(ha_end - halim))
            print "HAlimit: Adjusting end time to:", end_dt, (ha_end-halim)
            ha_end = halim
            block = (start_dt,end_dt)
        else:
            block = (start_dt,end_dt)
        blocks = [block]
        
        print
        print target['name'], lsplit[2], lsplit[4]
        print

        import sys
        sys.path.append('~/code/sqacontrol/')
        from transits import scheduleEvents
        if opt.interactive:
            opt.dry = True
            scheduleEvents(blocks, target, opt)
            print
            print target['name'], '%s (%3.1f) --> %s (%3.1f)' % (lsplit[2], ha_start, lsplit[4], ha_end), lsplit[17]
            print ' '.join(lsplit[18:])
            choice = raw_input("Enter 'yes' to schedule this event: ")
            if choice == 'yes':
                opt.dry = False
                scheduleEvents(blocks, target, opt)
        else: scheduleEvents(blocks, target, opt)
