#!/usr/bin/env python

import numpy as np
from datetime import datetime,timedelta
import sys
sys.path.append('/home/bfulton/code/mypond/')
from mypond import *
import time

cal_user = "bjfulton@lcogt.net"
cal_passwd = "87138104"

calendars = {
'1m0a.doma.bpl': 'lcogt.net_0ffpc2up79606cuadm57cu8c7c@group.calendar.google.com',
'0m4a.aqwa.bpl':'lcogt.net_f90o3c5atjhc783fd0vnoce2a0@group.calendar.google.com',

'0m8a.doma.sqa': 'lcogt.net_uoqnfhhciqt8dsmb85ca4sff74@group.calendar.google.com',

'1m0a.doma.elp':'lcogt.net_qoerdco4sljb3p5todgi2ej0u4@group.calendar.google.com',

'1m0a.doma.lsc':'lcogt.net_cakuu6jj5ebhdtja9fsn6g6rf0@group.calendar.google.com',
'1m0a.domb.lsc':'lcogt.net_14p6u40bgnk4m74dg6680hbkog@group.calendar.google.com',
'1m0a.domc.lsc':'lcogt.net_29hi9fucpkkp8cdmviulq5fa7o@group.calendar.google.com',

'1m0a.doma.cpt':'lcogt.net_aadadkn417nrg88nh63regopj8@group.calendar.google.com',
'1m0a.domb.cpt':'lcogt.net_i8dvg715aq62htou62trr94o4g@group.calendar.google.com',
'1m0a.domc.cpt':'lcogt.net_juc0qjjnuk0kk22d7baafrdrps@group.calendar.google.com',

'1m0a.doma.coj':'lcogt.net_jftg1g162eiuqfpvsqeinj4r2k@group.calendar.google.com',
'1m0a.domb.coj':'lcogt.net_q6ab9u6g13peuchocu0vt4cdnc@group.calendar.google.com',

#'0m8a.doma.sqa':'lcogt.net_jj6ahiuvrhkpamqe89qkh8gijk@group.calendar.google.com',          #temporary test calendar

}

def decompose(a_list):
    """Turns a list into a set of all elements and a set of duplicated elements.

    Returns a pair of sets. The first one contains elements
    that are found at least once in the list. The second one
    contains elements that appear more than once.

    >>> decompose([1,2,3,5,3,2,6])
    (set([1, 2, 3, 5, 6]), set([2, 3]))
    """
    return reduce(
        lambda (u, d), o : (u.union([o]), d.union(u.intersection([o]))),
        a_list,
        (set(), set()))

def GoogleCalUpload(obsdict,sdate,edate,dry=False,tel='1m0a',site='bpl',enc='doma'):
    import gdata.calendar.service
    import gdata.service
    import gdata.calendar
    from datetime import timedelta
    import atom

    calendar_service = gdata.calendar.service.CalendarService()
    calendar_service.email = cal_user
    calendar_service.password = cal_passwd
    calendar_service.source = 'Google-Calendar_Python_Sample-1.0'
    # Log into the calendar with the passed username/password
    calendar_service.ProgrammaticLogin()

    for obsid,block in zip(obsdict.values(),obsdict.keys()):
        gid = obsid[0]
        uid = obsid[1]
        content = obsid[2]
        if content != None: content = content.replace('\t','     ')
        title = gid+' - '+uid
        #print "Inserting event:",title,block[0],block[1].time()

        BPL_cal = calendars['%s.%s.%s' % (tel,enc,site)]
        query = gdata.calendar.service.CalendarEventQuery(BPL_cal,'private', 'full')
        query.start_min = (block[0]-timedelta(hours=1)).strftime('%Y-%m-%dT%H:%M:%S.000Z')
        query.start_max = (block[1]+timedelta(hours=1)).strftime('%Y-%m-%dT%H:%M:%S.000Z')
        # Set calender timezone to be UTC
        query.ctz = 'Etc/UTC'
        feed = calendar_service.CalendarQuery(query)

        block_start_str = block[0].strftime('%Y-%m-%dT%H:%M:%S.000Z')
        exists = False
        dup = False
        try:
            for i, an_event in enumerate(feed.entry):
                if len(an_event.when) == 0: continue
                     #print '\t%s. %s' % (i, an_event.title.text,)
                for a_when in an_event.when:
                    #print an_event.title.text.strip()
                    if (a_when.start_time == block_start_str) and (an_event.title.text.strip() == title.strip() or an_event.content.text.strip() == content):
                        if not exists:
                            print "%s already exists in the Google Cal." % title.strip(), a_when.start_time
                            exists = True
                        elif exists:
                            dup = True
                            print "%s has multiple entries in the Google Cal. Deleting..." % title.strip(), a_when.start_time
                            calendar_service = gdata.calendar.service.CalendarService()
                            calendar_service.email = cal_user
                            calendar_service.password = cal_passwd
                            calendar_service.source = 'Google-Calendar_Python_Sample-1.0'
                            calendar_service.ProgrammaticLogin()
                            try:
                                status = calendar_service.DeleteEvent(an_event.GetEditLink().href)
                                #print "deleted", status
                            except gdata.service.RequestError:
                                print "Error deleting from Google Cal. Trying 1 more time..."
                                time.sleep(3)
                                calendar_service = gdata.calendar.service.CalendarService()
                                calendar_service.email = cal_user
                                calendar_service.password = cal_passwd
                                calendar_service.source = 'Google-Calendar_Python_Sample-1.0'
                                calendar_service.ProgrammaticLogin()
                                status = calendar_service.DeleteEvent(an_event.GetEditLink().href)
                        #break
                    #else:
                    #    exists = False
                #if exists: break
        except UnboundLocalError:
            print "Feed is empty."
        
        if not exists:
            print "Inserting event:",title,block[0],block[1].time()
            event = gdata.calendar.CalendarEventEntry()
            event.title = atom.Title(text=title)
            event.content = atom.Content(text=content)
            event.where.append(gdata.calendar.Where(value_string='%s' % site))

            start_time = block[0].strftime('%Y-%m-%dT%H:%M:%S.000Z')
            end_time = block[1].strftime('%Y-%m-%dT%H:%M:%S.000Z')
            event.when.append(gdata.calendar.When(start_time=start_time, end_time=end_time))
            if not dry:
                for i in range(4):
                    try:
                        new_event = calendar_service.InsertEvent(event, feed.link[1].href)
                        print "Success!"
                        break
                    except gdata.service.RequestError:
                        print "Error uploading to Google Cal. Trying again..."
                        time.sleep(3)
                        calendar_service = gdata.calendar.service.CalendarService()
                        calendar_service.email = cal_user
                        calendar_service.password = cal_passwd
                        calendar_service.source = 'Google-Calendar_Python_Sample-1.0'
                        calendar_service.ProgrammaticLogin()
            else:
                print "Block %s already exists in calendar." % title

        #Check for orphaned events in the Google Cal
        block_start_strings = [block[0].strftime('%Y-%m-%dT%H:%M:%S.000Z') for block in obsdict.keys()]
        block_titles = [str(obsid[0]+' - '+obsid[1]) for obsid in obsdict.values()]
        try:
            #print "Checking for orphaned events"
            for i, an_event in enumerate(feed.entry):
                    if len(an_event.when) == 0: continue
                    #print '\t%s. %s' % (i, an_event.title.text,)
                    for a_when in an_event.when:
                        #print a_when.start_time, block_start_strings, an_event.title.text, block_titles
                        if (a_when.start_time not in block_start_strings or an_event.title.text not in block_titles) and datetime.strptime(a_when.start_time,'%Y-%m-%dT%H:%M:%S.000Z') > sdate:
                            print "No POND entry for event: %s, %s --> %s" % (an_event.title.text.strip(), a_when.start_time, a_when.end_time)
                            calendar_service = gdata.calendar.service.CalendarService()
                            calendar_service.email = cal_user
                            calendar_service.password = cal_passwd
                            calendar_service.source = 'Google-Calendar_Python_Sample-1.0'
                            calendar_service.ProgrammaticLogin()
                            try:
                                status = calendar_service.DeleteEvent(an_event.GetEditLink().href)
                                #print "deleted", status
                            except gdata.service.RequestError:
                                print "Error deleting from Google Cal. Trying 1 more time..."
                                time.sleep(3)
                                calendar_service = gdata.calendar.service.CalendarService()
                                calendar_service.email = cal_user
                                calendar_service.password = cal_passwd
                                calendar_service.source = 'Google-Calendar_Python_Sample-1.0'
                                calendar_service.ProgrammaticLogin()
                                status = calendar_service.DeleteEvent(an_event.GetEditLink().href)
        except UnboundLocalError:
            print "Feed is empty."
            time.sleep(2)

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Sync POND with the GoogleCals.',add_help=True)
    parser.add_argument('-d',dest='sdate',action='store',help='UT date of start of search window (YYYYMMDD)',default=datetime.strftime(datetime.utcnow(), format='%Y%m%d'),type=str)
    parser.add_argument('-n',dest='dry',action='store_true',help='Dry run. Print results, but do not upload to Google Cal.',default=False)
    parser.add_argument('--tel',dest='tel',action='store',help='Telescope',default='1m0a')
    parser.add_argument('--site',dest='site',action='store',help='Site',default='elp')
    parser.add_argument('--enc',dest='observatory',action='store',help='Observatory',default='doma')
    opt = parser.parse_args()

    sdate = datetime.strptime(opt.sdate,'%Y%m%d')
    edate = sdate + timedelta(days=1)

    start = sdate
    while sdate < start + timedelta(days=10):
        events,obsdict,link = getEvents(sdate,edate=edate,tel=opt.tel,site=opt.site,enc=opt.observatory)
        try:
            GoogleCalUpload(obsdict,sdate,edate,dry=opt.dry,tel=opt.tel,site=opt.site,enc=opt.observatory)
        except:
            print "Error syncing calendar for %s" % sdate
            pass
        sdate += timedelta(days=1)
        edate += timedelta(days=1)
