#!/usr/bin/env python

from rise_set.astrometry import calc_sunrise_set,Angle
from datetime import datetime,timedelta
import numpy as np
from lcogtpond import schedule
import lcogtpond
import sys
import astrolib as al

#Globals
elp = {'latitude':Angle(degrees=30.67),'longitude':Angle(degrees=-104.02),'name':'elp'}
sqa = {'latitude':Angle(degrees=34.687604),'longitude':Angle(degrees=-120.039067), 'name':'sqa'}
bpl = {'latitude':Angle(degrees=34.4325),'longitude':Angle(degrees=-119.863056), 'name':'bpl'}
lsc = {'latitude':Angle(degrees=-30.1675),'longitude':Angle(degrees=-70.804722), 'name':'lsc'}
cpt = {'latitude':Angle(degrees=-32.380609),'longitude':Angle(degrees=20.81009), 'name':'cpt'}

calibration_molecules = [lcogtpond.molecule.Bias,lcogtpond.molecule.Dark,lcogtpond.molecule.SkyFlat]

def pond2acp(block):
    m0 = block.molecules[0]
    head = """
;@ TAG_ID : %s
;@ PROPOSAL_ID : %s
;@ USER_ID : %s
;@ PRIORITY : %d\n""" % (m0.tag,m0.proposal,m0.user,block.priority)

    content = head

    for m in block.molecules:
        if type(m) == lcogtpond.molecule.Bias:
            acp = """
#BINNING %d
#COUNT %d
#BIAS\n""" % (m.bin,m.exp_cnt)
        
        elif type(m) == lcogtpond.molecule.Dark:
            acp = """
#BINNING %d
#COUNT %d
#INTERVAL %d
#DARK\n""" % (m.bin,m.exp_cnt,m.exp_time)
        
        elif type(m) == lcogtpond.molecule.SkyFlat:
            acp = """
#FILTER %s
#BINNING %d
#COUNT %d
#DUSKFLATS\n""" % (m.filters,m.bin,m.exp_cnt)
        
        else:
            acp = """
#FILTER %s
#BINNING %d
#COUNT %d
#INTERVAL %d
;TARGET_NAME\tTARGET_RA(hrs)\tTARGET_DEC(deg)
%s\t%f\t%f\n """ % (m.filters,m.bin,m.exp_cnt,m.exp_time,m.pointing.name,m.pointing.roll/15.0,m.pointing.pitch)

        try:
            if m.defocus > 0.01: acp += '\n #DEFOCUS %5.2f\n' % m.defocus
        except:
            pass

        content += acp

    return content

def getEvents(sdate,edate=None,debug=True,tel='1m0a',site='bpl',enc='doma'):

    start_str = sdate.strftime('%Y-%m-%d+%H%%3A%M%%3A%S')
    if edate == None: edate = sdate + timedelta(hours=16)
    end_str = edate.strftime('%Y-%m-%d+%H%%3A%M%%3A%S')

    link = 'http://scheduler.lco.gtn/pond/admin/pond/block/?&site=%s&observatory=%s&telescope=%s&start_gte=%s&start_lt=%s' % (site,enc,tel,start_str,end_str)
    print 'Scheduled blocks on %s.%s.%s for %s:' % (site,enc,tel,sdate.date())


    sched = schedule.Schedule.get(start=sdate,end=edate,site=site,observatory=enc,telescope=tel,canceled_blocks=False)
    
    events = []
    obsdict = {}
    for i,b in enumerate(sched.blocks):
        obs = {}
        
        #Check if block was cancelled
        if b._pb_obj.canceled:
            continue

        obs['start'] = b.start
        obs['end'] = b.end
        obs['block_obsid'] = b.id
        block = (obs['start'],obs['end'])
        events.append(block)

        try:
            first_molecule = b.molecules[0]
        except:
            #print "WARNING! Can not get molecule for block:", block
            continue
        gid = first_molecule.group
        uid = first_molecule.user
        priority = b.priority
        uid += ' - %d' % priority
        try:
            content = pond2acp(b)
        except:
            t,v,tb = sys.exc_info()
            
        obsdict[block] = [gid,uid,content]

        #Check for calibration frames
        if type(first_molecule) not in calibration_molecules:
            print "%s --> %s" % (b.start.time(),b.end.time()), gid, uid
        else:
            gid = repr(type(first_molecule)).split('.')[-1][:-2]
            obsdict[block] = [gid,uid,content]

    print
    return events,obsdict,link

def stripfrac(dt):
    return datetime(dt.year,dt.month,dt.day,dt.hour,dt.minute,dt.second)

def findFreeTime(cal_user="bjfulton@lcogt.net",cal_passwd="87138104",sdate=datetime.today(),debug=True,site=elp,tel='1m0a',enc='doma'):

    free = []
    twilight = 'nautical'
    noon,nightend,nightstart = calc_sunrise_set(site,sdate,twilight)
    nightstart += datetime(sdate.year,sdate.month,sdate.day)
    nightend += datetime(sdate.year,sdate.month,sdate.day)# + timedelta(days=1)
    midnight = datetime(sdate.year,sdate.month,sdate.day) + noon + timedelta(hours=0)
    nightstart = stripfrac(nightstart)
    nightend = stripfrac(nightend)
    if nightend < nightstart: nightend += timedelta(days=1)
    if debug:
        #print "Site: %s" % site['name']
        print "%s twilight begins at:" % twilight,nightstart
        #print "midnight at:", midnight
        print "%s twilight ends at:" % twilight,nightend
        print

    sdate = nightstart
    edate = nightend
    events,obsdict,link = getEvents(sdate,edate=edate,tel=tel,site=site['name'],enc=enc)
    events.append([nightend,nightend,'LCO','end'])
    events.insert(0,[nightstart,nightstart,'LCO','start'])

    #if nightstart < events[0][0]:
    #    nightstart += timedelta(days=1)
    #    nightend += timedelta(days=1)

    for i,e in enumerate(events):
        start_time = e[0]
        end_time = e[1]
        try:
            next_start = events[i+1][0]
        except IndexError:
            next_start = end_time
        if (next_start - end_time) > timedelta(minutes=5):
            free_block = (end_time,next_start)
            if free_block[0] > nightstart and free_block[1] < nightend: 
                pass
            elif free_block[0] < nightstart:
                free_block = (nightstart,free_block[1])
            elif free_block[1] > nightend:
                free_block = (free_block[0],nightend)
            if free_block[0] > free_block[1]:
                continue
            if debug: print "Found %4.2f hr free block: %s --> %s" % ((free_block[1] - free_block[0]).seconds/3600.,free_block[0].time(),free_block[1].time())
            free.append(free_block)
    
    print "\nLink to POND:", link
    print "\nLink to GoogleCalendar:", "http://www.google.com/calendar/embed?src=lcogt.net_qoerdco4sljb3p5todgi2ej0u4%40group.calendar.google.com&ctz=Etc/GMT\n"
    return free

def submitObs(target,block,user,proposal,site='bpl',tel='1m0a',enc='doma',inst='kb74',priority=40,dry=False):
    #print tel
    if tel == '1m0a' and site=='bpl':
        observatory = 'doma'
        inst = 'kb72'
    if tel == '0m4a' and site=='bpl':
        observatory = 'aqwa'
        inst = 'kb89'
    if tel == '1m0a' and site=='elp':
        observatory = 'doma'
        inst = 'kb74'
        #ag = 'en09'
        ag = 'ef01'
        ag = ''
    if tel == '1m0a' and site=='lsc' and enc=='doma':
        observatory = 'doma'
        inst = 'kb78'
        ag = 'en08'
        ag = ''
    if tel == '1m0a' and site=='lsc' and enc=='domb':
        observatory = 'domb'
        inst = 'kb73'
        ag = 'en07'
        ag = ''
    if tel == '1m0a' and site=='lsc' and enc=='domc':
        observatory = 'domc'
        inst = 'kb77'
        ag = ''
    if tel == '1m0a' and site=='cpt' and enc=='doma':
        observatory = 'doma'
        inst = 'kb70'
        ag = ''
    if tel == '1m0a' and site=='cpt' and enc=='domb':
        observatory = 'domb'
        inst = 'kb79'
        ag = ''
    if tel == '1m0a' and site=='cpt' and enc=='domc':
        observatory = 'domc'
        inst = 'kb75'
        ag = ''
    if tel == '0m8a' and site=='sqa':
        if 'w' in target['FILTER']:
            print "Replacing w filter with clear for SQA"
            newfilters = target['FILTER'].replace("w","clear")
            filt = newfilters
        if 'zs' in target['FILTER']:
            print "Replacing zs filter with zp for SQA"
            newfilters = target['FILTER'].replace("zs","zp")
            filt = newfilters
        observatory = 'doma'
        inst = 'kb16'
        ag = 'kb31'
    if tel == '1m0a' and site=='coj' and enc=='doma':
        observatory = 'doma'
        inst = 'kb05'
        ag = ''
    if tel == '1m0a' and site=='coj' and enc=='domb':
        observatory = 'domb'
        inst = 'kb71'
        ag = ''

    if not site=='sqa': ag = ''


    filtdef = {'ADNP-PI-002':'w','ADNP-BU-002':'B','ADNP-VX-002':'V','SDSS-GP-011':'gp','SDSS-RP-011':'rp','SDSS-IP-011':'ip','SDSS-ZP-011':'zs'}


    start = block[0]
    end = block[1]
    name = target['NAME']
    target_ra = target['RA']
    target_dec = target['DEC']
    try: filt
    except: filt = target['FILTER']
    if filt in filtdef.keys(): filt = filtdef[filt]
    exp = target['EXPTIME']
    binning = target['BINNING']
    try:
        defoc = target['defocus']
    except:
        defoc = 0.0

    if type(exp) != str and len(str(exp).split(',')) <= 1:
        #print type(exp), len(exp.split(','))
        cnt  = np.floor(((end - start).seconds - 300.0) / (float(exp) + 15.0))      #number of exposures
        rpt = 0
    else:
        cnt = []
        [cnt.append('1') for f in range(len(filt.split(',')))]
        cnt = ','.join(cnt)
        total_exp = np.sum(np.array(exp.split(','),dtype=float)) + 15.0*len(exp.split(','))
        rpt = np.round(((end - start).seconds - 300.0) / total_exp)
        rpt = int(rpt)
        if rpt < 1: rpt = 1
        
    
    if str(target['RA']).find(':') != -1:
        target_ra,target_dec = al.sex2deg(target['RA'],target['DEC'])
        target['RA'] = target_ra
        target['DEC'] = target_dec
    
    target_ra = float(target_ra)
    target_dec = float(target_dec)

    print "Scheduling observations to %s.%s.%s:" % (site,enc,tel)
    print start,'-->', end
    print "   %s:\t%s" % ('NAME',name)
    print "   %s:\t\t%s" % ('RA',target['RA'])
    print "   %s:\t\t%s" % ('DEC',target['DEC'])
    print "   %s:\t%s" % ('EXPOSURE',exp)
    print "   %s:\t%s" % ('COUNT',cnt)
    print "   %s:\t%s" % ('FILTER',filt)
    print "   %s:\t%s" % ('BINNING',binning)
    print "   %s:\t%s" % ('DEFOCUS',defoc)
    print
    
    block_params = {
	'start' : start,
	'end' :   end,
	'site' : site,
	'observatory' : observatory,
	'telescope' : tel,
	'priority' : priority
        }
    
    block = lcogtpond.block.Block.build(**block_params)
    #point_params = {'name': name,'ra':float(target['RA']), 'dec':float(target['DEC'])}
    point_params = {'ra':float(target['RA']), 'dec':float(target['DEC'])}
    coords = lcogtpond.pointing.ra_dec(**point_params)
    pondtarget = lcogtpond.pointing.sidereal(name=name,coord=coords)

    expose_params = {
        'tag' : 'LCOGT',
	'user' : user,
	'proposal' : proposal,
        'group' : name,
	
        'pointing' : pondtarget,
        'inst_name' : inst,
	'ag_mode' : 2,
	'ag_name' : ag,
        'bin' : binning,
        'filters' : filt,
        'exp_time' : exp,
        'defocus' : defoc
        }

    print expose_params['bin']
    if type(expose_params['bin']) == str or len(str(expose_params['bin']).split(',')) > 1:
        filters = np.array(expose_params['filters'].split(','),dtype=str)
        binnings = np.array(expose_params['bin'].split(','),dtype=int)
        exptimes = np.array(expose_params['exp_time'].split(','),dtype=float)
        print filters,binnings,exptimes

        expose_priority = 0
        for j in range(rpt):
            for i,filt in enumerate(filters):
                new_params = {'exp_cnt' : int(cnt[0]), 'exp_time':exptimes[i], 'bin':binnings[i], 'filters':filters[i],'priority':expose_priority}
                expose_params.update(new_params)
                expose = lcogtpond.molecule.Expose.build(**expose_params)
                block.add_molecule(expose)
                expose_priority += 1
            
    else:
        new_params = {'exp_cnt' : int(cnt),'exp_time' : float(exp), 'bin': int(binning), 'priority' : 0}
        expose_params.update(new_params)
        expose = lcogtpond.molecule.Expose.build(**expose_params)
        block.add_molecule(expose)

    if not dry: block.save()
