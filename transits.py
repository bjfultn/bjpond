#!/usr/bin/env python

from datetime import datetime,timedelta
import numpy as np
from rise_set.astrometry import *
import sys
import argparse
import numpy
import os
#from plansetup import getEvents
import astrolib as al
from mypond import *
from sites import *


cal_user = "bjfulton@lcogt.net"
cal_passwd = "87138104"
sdate = datetime.today() + timedelta(days=1)
debug = True
dead_time = 22.0     #exposure overhead
owner = 'bj'


twilight = 'nautical'

def grabconfig(configfile):
    import ConfigParser
    #Read config file and set variables
    config = ConfigParser.SafeConfigParser()
    config.read(configfile)
    out = {}
    out['period'] = float(config.get('ephem','period'))
    out['t0'] = float(config.get('ephem','t0'))
    out['duration'] = float(config.get('ephem','duration'))
    #out['days'] = int(config.get('ephem','days'))
    out['ra'] = config.get('obs','ra')
    out['dec'] = config.get('obs','dec')
    out['exp'] = float(config.get('obs','exp'))
    out['exptime'] = float(config.get('obs','exp'))
    out['binning'] = int(config.get('obs','binning'))
    out['filter'] = config.get('obs','filter')
    out['name'] = os.path.basename(configfile).split('.')[0]

    out['owner'] = config.get('admin','owner')
    out['proposal'] = config.get('admin','proposal')

    for k in out.keys():
        out[k.upper()] = out[k]
    
    return out

def makeACPobs(target,free_block):
    #content = "#WAITUNTIL 1, " + free_block[0].strftime('%d-%b-%Y %H:%M:%S') + "\n"
    content = "#AUTOGUIDE\n"
    content += "#BINNING %d\n" % target['binning']
    content += "#FILTER %s\n" % target['filter']
    content += "#INTERVAL %d\n" % target['exp']
    count = np.floor(((free_block[1] - free_block[0]).seconds - 300.0) / (target['exp']+dead_time))
    content += "#COUNT %d\n" % count
    content += "%s     %s     %s\n" % (target['name'],str(target['ra']),str(target['dec']))

    if debug: print content
    if count <= 0: return -1
    else: return content

def isup(ha_start,ha_mid,ha_end,allowpartial=True):
    if allowpartial: req = 2
    else: req = 3

    isup = (int(np.abs(ha_start) < halim) + int(np.abs(ha_mid) < halim) + int(np.abs(ha_end) < halim)) >= req
    return isup

def isdark(start_dt,event_dt,end_dt,allowpartial=True):
    date = datetime(event_dt.year,event_dt.month,event_dt.day)
    noon,nightend,nightstart = calc_sunrise_set(site,date,twilight)

    nightstart += date
    nightend += date
    if allowpartial: req = 2
    else: req = 3

    if nightstart > nightend: nightstart -= timedelta(days=1)

    isdark = (int(start_dt > nightstart and start_dt < nightend) + int(event_dt > nightstart and event_dt < nightend) + int(end_dt > nightstart and end_dt < nightend)) >= req

    if isdark:
        print "%s: %s twilight begins at:" % (site['name'],twilight),nightstart
        print "%s: %s twilight ends at:" % (site['name'],twilight),nightend
        pass

    return isdark

def calcEvents(sdate,days,target,allowpartial=True):
    p = target['period']
    t0 = target['t0']
    if opt.secondary: t0 += p/2.
    dur = target['duration']
    jdstart = al.date2jd(sdate)
    jdend = jdstart + days
    jd_array = np.arange(jdstart,jdend,0.003472)
    events = []
    blocks = []
    event = t0
    while event <= jdstart:
        event += p
    while event <= jdend:
        event += p
        event_dt = al.jd2date(event)
        start_dt = event_dt - timedelta(hours=dur/2.)#+pad)
        end_dt = event_dt + timedelta(hours=dur/2.)#+pad)
        block = (start_dt,end_dt)

        ha_start = al.hourAngle(target,start_dt,site)
        ha_end = al.hourAngle(target,end_dt,site)
        ha_mid = al.hourAngle(target,event_dt,site)
        up = isup(ha_start,ha_mid,ha_end,allowpartial)

        dark = isdark(start_dt,event_dt,end_dt,allowpartial)
        print up,dark, ha_start, ha_end, block[0], block[1]
        if up and dark:
            start_dt -= timedelta(hours=pad)
            end_dt += timedelta(hours=pad)
            ha_start = al.hourAngle(target,start_dt,site)
            ha_end = al.hourAngle(target,end_dt,site)
            if ha_start < -halim:
                start_dt -= timedelta(hours=(ha_start + halim))
                print "HAlimit: Adjusting start time to:", start_dt, (ha_start+halim)
                ha_start = -halim
                block = (start_dt,end_dt)
            elif ha_end > halim:
                end_dt -= timedelta(hours=np.abs(ha_end - halim))
                print "HAlimit: Adjusting end time to:", end_dt, (ha_end-halim)
                ha_end = halim
                block = (start_dt,end_dt)
            else:
                block = (start_dt,end_dt)
            print "N:", int((event-t0) / p)
            print "JD of mid-transit time:",event
            print "Time of block (start center end):",start_dt,event_dt.time(),end_dt.time()
            print "HA (start center end):" ,ha_start, ha_mid, ha_end,"\n"
            events.append(event)
            blocks.append(block)

    return blocks

def scheduleEvents(blocks, target, opt):
    from mypond import *
    from lcosched import submitObs, submitObs_pond

    targetname = '%s - %s' % (target['name'],target['owner'])
    site = opt.site
    
    try:
        twilight = opt.twilight
    except:
        print "Using default nautical twilight"
        twilight = 'nautical'

    for block in blocks:
        block = list(block)
        sdate = datetime(block[0].year,block[0].month,block[0].day)
        if opt.all:
            content = makeACPobs(target,block)
            if not opt.dry:
                submitObs(target,block,target['owner'],target['proposal'],site=site['name'],enc=opt.enc,tel=opt.tel,priority=opt.priority)
        else:
            if opt.site['name'] == 'sqa':
                from mypond import submitObs
                print "transits.scheduleEvents.sdate=", sdate
                free = findFreeTime(sdate=sdate,site=site,debug=True,tel=opt.tel, enc=opt.enc)
            else:
                free = findFreeTime(sdate=sdate,site=site,debug=True,tel=opt.tel, enc=opt.enc)
            for f in free:
                print "Free:",f[0],f[1]," Desired:",block[0],block[1]
                if block[0] > f[1] or block[1] < f[0]:
                    print "Block not contained in free slot."
                    continue
                if block[0] > f[0] and block[1] < f[1]:
                    print "Can schedule full block."
                elif block[0] > f[0] and block[1] > f[1]:
                    print "Can schedule first part of block."
                    block[1] = f[1]
                elif block[0] < f[0] and block[1] < f[1]:
                    print "Can schedule second part of block."
                    block[0] = f[0]
                elif block[0] < f[0] and block[1] > f[1]:
                    print "Can schedule middle of block."
                    block[0] = f[0]
                    block[1] = f[1]
                if (block[1] - block[0]) < timedelta(hours=target['duration'] * 0.40):
                    print "Block length too short (%d < %d min)" % ((block[1] - block[0]).seconds / 60, timedelta(hours=target['duration']).seconds / (2*60))
                    continue
                
                print "Scheduling block:",block[0],block[1]
                content = makeACPobs(target,block)
                if content == -1:
                    print "Negative exposure counts. Block probably too short."
                    continue
                if not opt.dry:
                    print opt.site['name'], opt.site['name']=='sqa'
                    if opt.site['name'] == 'sqa':
                        print "Submitting directly to SQA POND"
                        submitObs_pond(target,block,target['owner'],target['proposal'],site=site['name'],enc=opt.enc,tel=opt.tel,priority=opt.priority)
                    else:
                        submitObs(target,block,target['owner'],target['proposal'],site=site['name'],enc=opt.enc,tel=opt.tel,priority=opt.priority)
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='Enter transit observations into the BOS Google Cal',add_help=True)
    parser.add_argument('-f',dest='file',action='store',help='Target file.',type=str)
    parser.add_argument('-s',dest='days',action='store',help='Search N days from start date.',default=7,type=int)
    parser.add_argument('-d',dest='sdate',action='store',help='UT date of start of search window (YYYYMMDD)',default=datetime.strftime(datetime.utcnow() - timedelta(days=1), format='%Y%m%d'),type=str)
    parser.add_argument('--site',dest='site',action='store',help='Site.',default='sqa',type=str)
    parser.add_argument('-n',dest='dry',action='store_true',help='Dry run. Print results, but do not upload to Google Cal.',default=False)
    parser.add_argument('-a',dest='all',action='store_true',help='Schedule all events regardless of existing blocks.',default=False)
    parser.add_argument('-p','--pad',dest='pad',action='store',help='Length of OoT observations on either side of transit.',default=1.0,type=float)
    parser.add_argument('-o','--owner',dest='own',action='store',help='Owner of events.',default=owner,type=str)
    parser.add_argument('--proposal',dest='propid',action='store',help='Propsal ID.', default='none',type=str)
    parser.add_argument('--priority',dest='priority',action='store',help='priority', default=40,type=int)
    parser.add_argument('--nopartial',dest='part',action='store_false',help='Force full event visibility.',default=True)
    parser.add_argument('--secondary',dest='secondary',action='store_true',help='Search for secondary eclipses.',default=False)
    parser.add_argument('--tel',dest='tel',action='store',help='Telescope',default='1m0a')
    parser.add_argument('--enc',dest='enc',action='store',help='Observatory',default='doma')

    opt = parser.parse_args()

    sdate = datetime.strptime(opt.sdate,'%Y%m%d')

    target = grabconfig(opt.file)
    try:
        target['owner']
    except KeyError:
        target['owner'] = opt.own
    try:
        target['proposal']
    except KeyError:
        target['proposal'] = opt.propid
    pad = opt.pad

    site = allsites[opt.site]
    opt.site = site

    dead_time = 15.0
    halim = 5.0

    blocks = calcEvents(sdate,opt.days,target,allowpartial=opt.part)
    if not opt.dry:
        scheduleEvents(blocks, target, opt)
