#!/usr/bin/env python

from datetime import datetime,timedelta
import numpy as np
from rise_set.astrometry import *
import sys
import argparse
from rise_set.astrometry import calc_sunrise_set,Angle
import astrolib as al
from mypond import *
from sites import *

cal_user = "bjfulton@lcogt.net"
cal_passwd = "87138104"
debug = True
dead_time = 15.0     #exposure overhead

sqa = {'latitude':Angle(degrees=34.687604),'longitude':Angle(degrees=-120.039067), 'name':'sqa'}
bpl = {'latitude':Angle(degrees=34.4325),'longitude':Angle(degrees=-119.863056), 'name':'bpl'}
elp = {'latitude':Angle(degrees=30.67),'longitude':Angle(degrees=-104.02),'name':'elp'}
lsc = {'latitude':Angle(degrees=-30.1675),'longitude':Angle(degrees=-70.804722), 'name':'lsc'}   
cpt = {'latitude':Angle(degrees=-32.380609),'longitude':Angle(degrees=20.81009), 'name':'cpt'}


def readtargets(targetfile):
    targets = np.genfromtxt(targetfile,names=True,dtype=None)
    return targets

def makeACPobs(target,free_block):
    #content = "#WAITUNTIL 1, " + free_block[0].strftime('%d-%b-%Y %H:%M:%S') + "\n"
    content = "#AUTOGUIDE\n"
    try:
        if target[9] > 0.:
            content += "#DEFOCUS %5.2f\n" % target[9]
    except NameError: pass
    content += "#BINNING %s\n" % target[4]
    content += "#FILTER %s\n" % target[3]
    content += "#INTERVAL %s\n" % target[5]
    if len(target[3].split(',')) == 1:
        count = np.round(((free_block[1] - free_block[0]).seconds - 300.0) / (float(target[5])+dead_time))
        count = int(count)
        if count < 1: count = 1
    else:
        total_exp = np.sum(np.array(target[5].split(','),dtype=float)) + dead_time*len(target[5])
        rpt = np.round(((free_block[1] - free_block[0]).seconds - 300.0) / total_exp)
        count = []
        [count.append('1') for f in target[3].split(',')]
        count = ','.join(count)
        content = content.replace('#AUTOGUIDE\n','#AUTOGUIDE\n#REPEAT %d\n' % rpt)

    content += "#COUNT %s\n" % count
    content += "%s     %s     %s\n" % (target[0],str(target[1]),str(target[2]))

    if debug: print content
    
    return content
    
def main(site,tel,enc,force=False,split=False,rerun=False,priority=40):
    free = findFreeTime(cal_user,cal_passwd,sdate,debug=debug,site=site,tel=tel,enc=enc)
    targets = readtargets(targetfile=opt.file)

    for i,block in enumerate(free):
        block_dur = (block[1] - block[0]).seconds / 3600.
        block_mid = block[0] + (block[1] - block[0]) / 2

    for block in free:
        block_mid = block[0] + (block[1] - block[0]) / 2
        block_dur = (block[1] - block[0]).seconds / 3600.

        if split and rerun and block_dur > 2.0:
                print "Splitting block in two:", block
                free.append((block_mid,block[1]))
                block = (block[0], block_mid)
                free[i] = block
                split = False


        iblock = (block[0],block[1])
        if debug: print "Center of block:", block_mid
        if debug: print "Block Duration:", block_dur
        if block[0] < datetime.utcnow():
            print "Start of block before current time."
            block = (datetime.utcnow(), block[1])
        if block[1] < datetime.utcnow():
            print "End of block before current time."
            continue
        bestHA = 999
        best_target = None
        for i,t in enumerate(targets):
            HA_mid = al.hourAngle(t,block_mid,site=site)
            HA_start = al.hourAngle(t,iblock[0],site=site)
            HA_end = al.hourAngle(t,iblock[1],site=site)
            if np.abs(HA_start) > 4.8 or np.abs(HA_end) > 4.8:
                print "%s outside limits for part of block." % t['NAME'], HA_start, HA_end
                if force and not (HA_start > HA_end):                          
                    if np.abs(HA_start) > 4.8:
                        print iblock[0],iblock[1]
                        block = (iblock[0] + timedelta(hours=np.abs(HA_start) - 4.8), iblock[1])                
                        best_target = t
                        block_dur = (block[1] - block[0]).seconds / 3600.
                        print "Forcing target %s by adjusting start time to %s" % (t['NAME'],block[0])
                        
                    elif np.abs(HA_end) > 4.8:
                        print iblock[0],iblock[1]
                        block = (iblock[0], iblock[1] - timedelta(hours=np.abs(HA_end) - 4.8))                
                        best_target = t
                        block_dur = (block[1] - block[0]).seconds / 3600.
                        print "Forcing target %s by adjusting end time to %s" % (t['NAME'],block[1])
                        
                else:                                                                         
                    continue
            elif t['MIN_DURATION'] > block_dur:
                print "Block duration too short for %s" % t['NAME']
                continue
            elif np.abs(HA_mid) < np.abs(bestHA):
                best_target = t
                bestHA = HA_mid

        if best_target == None:
            print "Could not find target for block.\n"
            print rerun, block_dur
            if not rerun and block_dur > timedelta(minutes=3).seconds/3600.:
                main(site,tel,enc,split=True,rerun=True)
            else: continue
            #continue
        rerun = True
        if debug:
            HA_start = al.hourAngle(t,block[0],site=site)                                                                                    
            HA_end = al.hourAngle(t,block[1],site=site)
            try: print "Selected %s as best target for block." % best_target['NAME'], block[0], block[1]
            except: continue
        acp_string = makeACPobs(best_target,block)
        try: user = best_target[7]
        except IndexError:user = opt.owner
        try: prop = best_target[8]
        except IndexError: prop = opt.proposal
        #print best_target
        if not opt.dry:
            print "Submitting observations to POND..."
            block = (block[0] + timedelta(seconds=60),block[1])
            submitObs(best_target,block,user,prop,site=site['name'],tel=tel,enc=enc,priority=priority)
            

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Enter observations into the BOS Google Cal')
    parser.add_argument('-d',dest='sdate',action='store',help='UT date of intented observations (YYYYMMDD)',default=datetime.strftime(datetime.utcnow(), format='%Y%m%d-%H:%M:%S'),type=str)
    parser.add_argument('-n',dest='dry',action='store_true',help='Dry run. Print results, but do not upload to Google Cal.',default=False)
    parser.add_argument('-f',dest='file',action='store',help='File containing target list.',default='/home/bfulton/code/mypond/bgtargets.dat',type=str)
    parser.add_argument('-o',dest='owner',action='store',help='Owner of events.',default='bj.fulton',type=str)
    parser.add_argument('-p',dest='proposal',action='store',help='Proposal ID',default='LCOELP-102',type=str)
    parser.add_argument('--site',dest='site',action='store',help='Site',default='elp',type=str)
    parser.add_argument('--tel',dest='tel',action='store',help='Telescope',default='1m0a')
    parser.add_argument('--enc',dest='enc',action='store',help='Observatory',default='doma')
    parser.add_argument('--force',dest='force',action='store_true',help='Adjust start and end time to HA+-5.',default=False)
    parser.add_argument('--priority',dest='priority',action='store',help='Priority',default=40, type=int)

    global opt
    opt = parser.parse_args()
    if opt.force: split = False
    else: split = True

    targetfile = opt.file

    sdate = datetime.strptime(opt.sdate,'%Y%m%d-%H:%M:%S')

    site = eval(opt.site)
    #if site['longitude'].degrees > 0: sdate -= timedelta(days=1)

    main(site,opt.tel,opt.enc,force=opt.force, split=split,priority=opt.priority)
