#!/usr/bin/env python

from rise_set.astrometry import *

sqa = {'latitude':Angle(degrees=34.687604),'longitude':Angle(degrees=-120.039067),'name':'sqa', 'obstr':'34.69139%3B-120.04222%3BPST8PDT%3BByrne+Observatory+Sedgwick%2C+California+%28BOS%29'}
bpl = {'latitude':Angle(degrees=34.4325),'longitude':Angle(degrees=-119.863056),'name':'bpl'}
ogg = {'latitude':Angle(degrees=20.7075),'longitude':Angle(degrees=-156.256111),'name':'ogg'}
coj = {'latitude':Angle(degrees=-31.2733),'longitude':Angle(degrees=149.071111),'name':'coj'}
elp = {'latitude':Angle(degrees=30.67),'longitude':Angle(degrees=-104.02),'name':'elp','obstr': '30.67%3B-104.02%3BCST6CDT%3BMcDonald+Observatory+%28ELP%29&'}
lsc = {'latitude':Angle(degrees=-30.1675),'longitude':Angle(degrees=-70.804722), 'name':'lsc', 'obstr': '-30.169%3B-70.804%3BAmerica%2FSantiago%3BCerro+Tololo%2C+Chile+%28LSC%29'}
cpt = {'latitude':Angle(degrees=-32.380609),'longitude':Angle(degrees=20.81009), 'name':'cpt', 'obstr': '-32.38%3B20.81%3BAfrica%2FJohannesburg%3BSouth+African+Astronomical+Observatory+%28CPT%29'}
allsites = {'sqa':sqa,'bpl':bpl,'ogg':ogg,'coj':coj,'elp':elp,'lsc':lsc, 'cpt':cpt}
