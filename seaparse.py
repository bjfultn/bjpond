#!/usr/bin/env python

import urllib2, base64
import argparse
from datetime import datetime, timedelta
from sites import *
import astrolib as al
import numpy as np

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parse and schedule KELT obs into POND')
    parser.add_argument('-d',dest='sdate',action='store',help='UT date of intented observations (YYYYMMDD)',default=datetime.strftime(datetime.today()+timedelta(days=1), format='%Y%m%d'),type=str)
    parser.add_argument('-s',dest='days',action='store',help='Search N days from start date.',default=1,type=int) 
    parser.add_argument('-n',dest='dry',action='store_true',help='Dry run. Print results, but do not upload to Google Cal.',default=False)
    parser.add_argument('-a',dest='all',action='store_true',help='Schedule all events regardless of existing blocks.',default=False)
    parser.add_argument('--inter',dest='interactive',action='store_true',help='Interactive?.',default=False)
    parser.add_argument('-f',dest='filter',action='store',help='Filter',default='ip',type=str)
    parser.add_argument('-o',dest='owner',action='store',help='Owner of events.',default='bj.fulton',type=str)
    parser.add_argument('-p',dest='proposal',action='store',help='Proposal ID',default='LCOELP-102',type=str)
    parser.add_argument('--priority',dest='priority',action='store',help='priority', default=40,type=int)
    parser.add_argument('--site',dest='site',action='store',help='Site',default='elp',type=str)
    parser.add_argument('--tel',dest='tel',action='store',help='Telescope',default='1m0a')
    parser.add_argument('--enc',dest='enc',action='store',help='Observatory',default='doma')
    parser.add_argument('--pad',dest='pad',action='store',help='Length of OoT observations on either side of transit. (hours)',default=0.0,type=float)
    parser.add_argument('--twilight',dest='twilight',action='store',help='Twilight (sunset|civil|nautical|astronomical)',default='civil')
    


    global opt
    opt = parser.parse_args()
    owner = opt.owner
    proposal = opt.proposal
    priority = 51

    halim = 5.0

    sdate = datetime.strptime(opt.sdate,'%Y%m%d')
    sdate_str = datetime.strftime(sdate, '%m-%d-%Y')

    site = eval(opt.site)
    opt.site = site
    opt.obs = opt.enc
    if site['longitude'].degrees > 0: sdate -= timedelta(days=1)

    if site['name'] == 'sqa': predicts = open('BOS_JulyAug2013.txt','r')
    if site['name'] == 'elp': predicts = open('ELP_JulyAug2013.txt','r')

    cands = np.genfromtxt('cand_info.txt', usecols=(1,2,3,4,5,6))
    names = np.loadtxt('cand_info.txt', dtype=str, usecols=(0,))
    
    for line in predicts.readlines():
        try:
            x = int(line[1])
            if line.startswith('#'): continue
        except:
            continue
        #if line[0] not in range(10,dtype=str): continue
        target = {}
        lsplit = np.array(line.split(' '))
        lsplit = lsplit[np.where(lsplit != '')[0]]
        target['NAME'] = target['name'] = lsplit[0]
        #coords = lsplit[15].split(' ')
        #print np.where(names == target['NAME'])[0]
        try:
            target['ra'] = target['RA'] = cands[:,0][np.where(names == target['NAME'])[0]][0]
            target['dec'] = target['DEC'] = cands[:,1][np.where(names == target['NAME'])[0]][0]
        except IndexError:
            print "WARNING: Can't find %s in cand_info.txt" % target['name']
            continue
            
        target['filter'] = target['FILTER'] = opt.filter
        target['binning'] = target['BINNING'] = 2
        target['owner'] = owner
        target['proposal'] = proposal
        target['priority'] = priority
        target['defocus'] = 0.0

        m0 = 12.0
        if opt.filter == 'zs':
            m0 = 10.5
            target['defocus'] = 2.0
        if site == sqa: m0 -= 0.5
        mag = cands[:,3][np.where(names == target['NAME'])[0]][0]
        deltamag = mag - m0
        exp = 90.0 * 10 ** (deltamag/2.5)

        target['exp'] = target['EXPTIME'] = round(exp)
        while exp < 30:
            target['defocus'] += 1.0
            target['exp'] = target['EXPTIME'] = exp = exp + 10.0

        minutes = (float(lsplit[-3]) - np.floor(float(lsplit[-3]))) * 60.
        start_dt = datetime(int(lsplit[3]), int(lsplit[4]), int(lsplit[-4]), int(np.floor(float(lsplit[-3]))), int(minutes), 0)
        #if start_dt - datetime.utcnow() > timedelta(days=1+opt.days):
        if start_dt - sdate > timedelta(days=1+opt.days):
            print "Block too far in advance: %s" % start_dt
            continue

        minutes = (float(lsplit[-2]) - np.floor(float(lsplit[-2]))) * 60.
        end_dt = datetime(int(lsplit[3]), int(lsplit[4]), int(lsplit[-4]), int(np.floor(float(lsplit[-2]))), int(minutes), 0)
        if end_dt < datetime.utcnow():
            print "End of block in past: %s" % end_dt
            continue

        opt.sdate = start_dt
        dur = end_dt - start_dt
        target['duration'] = dur.seconds / 3600.
        #ha_start = lsplit[12]
        #ha_end = lsplit[14]

        start_dt -= timedelta(hours=opt.pad)
        end_dt += timedelta(hours=opt.pad)
        ha_start = al.hourAngle(target,start_dt,site)
        ha_end = al.hourAngle(target,end_dt,site)
        if ha_start < -halim:
            start_dt -= timedelta(hours=(ha_start + halim))
            print "HAlimit: Adjusting start time to:", start_dt, (ha_start+halim)
            ha_start = -halim
            block = (start_dt,end_dt)
        elif ha_end > halim:
            end_dt -= timedelta(hours=np.abs(ha_end - halim))
            print "HAlimit: Adjusting end time to:", end_dt, (ha_end-halim)
            ha_end = halim
            block = (start_dt,end_dt)
        else:
            block = (start_dt,end_dt)
        blocks = [block]
        
        print
        print target['name'], lsplit[2], lsplit[4]
        print

        import sys
        from transits import scheduleEvents
        if opt.interactive:
            opt.dry = True
            print "seaparse.sdate=", sdate
            scheduleEvents(blocks, target, opt)
            print
            print target['name'], '%s --> %s' % (start_dt, end_dt), lsplit[1]
            print ' '.join(lsplit[18:])
            choice = raw_input("Enter 'yes' to schedule this event: ")
            if choice == 'yes':
                opt.dry = False
                scheduleEvents(blocks, target, opt)
        else:
            pass
            #scheduleEvents(blocks, target, opt)
        
