#!/opt/epd/bin/python
#does observations on std-field in specified filter, dithered by dith-arcsec
#start is start time in UT
#SET: date/time, std-field, coords, filter (exposure)

from datetime import timedelta,datetime

tag = 'LCOGT'
user = 'bj.fulton'
proposal = 'LCOELP-102'
priority = 39
inst = 'kb16'              #instrument/CCD
ag = 'kb16'
defocus = 0.0       #override this later if you wish
site = 'sqa'
domb = 'a'
ndays = 1

start = datetime( 2013, 5, 14, 6, 00, 0, 0)
end = datetime( 2013, 5, 14, 12, 00, 0, 0)

name = '15550+4224'
target_ra = 238.77221
target_de = 42.41086
filt = ['ip']   #filter, dashes
exp = [90]
bin  = [2]     #binning
cnt = [223]
rpt = 1     #number of repeats
#defocus = 1.5

name = '17228+3748'
target_ra = 260.72316
target_de = 37.80233
filt = ['ip']   #filter, dashes
exp = [90]
bin  = [2]     #binning
cnt = [223]
rpt = 1     #number of repeats
#defocus = 1.5

print locals()

def makeObs(ra, dec, **vars):
	target_ra = ra
	target_de = dec

	from lcogtpond import block,molecule,pointing

	from datetime import datetime, timedelta
	if site == 'sqa': tel = '0m8a'
	else: tel = '1m0a'
	block_params = {
	'start' : start,  #yyyy,mm,dd,hh,mm,ss,ms
	'end' :   end,
	'site' : site,
	'observatory' : 'dom'+domb,
	'telescope' : tel,
	'priority' : priority
	}
	block_constraint_params = {
		'airmass' : 2.5,
		'trans' : 0.8,
		'seeing' : 5.0,
		'lunar_dist' : 36000,
		'lunar_phase' : 1.0
		}

	#block_params.update(block_constraint_params)
	block = block.Block.build(**block_params)

	if str(target_ra).find(':') != -1:
		ra_sex = [float(r) for r in target_ra.split(':')]
		ra_deg = ra_sex[0] + ra_sex[1]/60.0 + ra_sex[2]/3600.0
		ra_hr = ra_sex[0] + ra_sex[1]/60.0 + ra_sex[2]/3600.0
		ra_deg = ra_hr * 15.0
		dec_sex = [float(d) for d in target_de.split(':')]
		if dec_sex[0] > 0: dec_deg = dec_sex[0] + dec_sex[1]/60.0 + dec_sex[2]/3600.0
		else: dec_deg = dec_sex[0] - dec_sex[1]/60.0 - dec_sex[2]/3600.0
		
		target_ra = ra_deg
		target_de = dec_deg

	grid = '%s' % (name)
	ra = target_ra  #ignore cos(dec) for equatorial standards
	de = target_de
	point_params = {'ra':ra, 'dec':de}
	coords = pointing.ra_dec(**point_params)
	sid_target = pointing.sidereal(name=grid,coord=coords)

	expose_params = {
	'tag' : tag,
	'user' : user,
	'proposal' : proposal,
	'group' : name,
	'inst_name' : inst,
	'pointing' : sid_target,
	'ag_mode' : 0,
	'ag_name' : ag,
	'defocus' : defocus}
	expose_priority = 0  # get observations in order
	for j in range(rpt):
		for i in range(len(filt)):          
			print grid, ra, de, filt[i], bin[i], cnt[i]
			new_params = {'exp_cnt':cnt[i],'exp_time':exp[i], 'bin':bin[i], 'filters':filt[i],'priority':expose_priority}
			expose_params.update(new_params)
			expose = molecule.Expose.build(**expose_params)
			block.add_molecule(expose)
			expose_priority += 1

	block.save()


for i in range(ndays):
	makeObs(target_ra, target_de, **locals())
	start += timedelta(days=1)
	end += timedelta(days=1)
