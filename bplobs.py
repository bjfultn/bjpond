#!/usr/bin/env python

import numpy as np
from datetime import datetime,timedelta
#from lcogt.pond import pond_client
#pond_client.configure_service('devlin1sba.lco.gtn', 12345)
from mypond import getEvents
import httplib
import time

cal_user = "bjfulton@lcogt.net"
cal_passwd = "87138104"

calendars = {'1m0a.doma.bpl': 'lcogt.net_0ffpc2up79606cuadm57cu8c7c@group.calendar.google.com', '0m4a.aqwa.bpl':'lcogt.net_f90o3c5atjhc783fd0vnoce2a0@group.calendar.google.com','1m0a.doma.elp':'lcogt.net_qoerdco4sljb3p5todgi2ej0u4@group.calendar.google.com'}

filtdef = {'ADNP-PI-002':'w','ADNP-BU-002':'B','ADNP-VX-002':'V','SDSS-GP-011':'gp','SDSS-RP-011':'rp','SDSS-IP-011':'ip','SDSS-ZP-011':'zs'}

def date2jd(date):
    jd_td = date - datetime(2000,1,1,12,0,0)
    jd = 2451545.0 + jd_td.days + jd_td.seconds/86400.0
    return jd

def jd2date(jd):
    mjd = jd - 2400000.5
    td = timedelta(days=mjd)
    dt = datetime(1858,11,17,0,0,0) + td

    return dt

def GoogleCalUpload(obsdict,dry=False,tel='1m0a',site='bpl'):
    import gdata.calendar.service
    import gdata.service
    import gdata.calendar
    from datetime import timedelta
    import atom

    if tel == '1m0a': enc = 'doma'
    if tel == '0m4a': enc = 'aqwa'

    calendar_service = gdata.calendar.service.CalendarService()
    calendar_service.email = cal_user
    calendar_service.password = cal_passwd
    calendar_service.source = 'Google-Calendar_Python_Sample-1.0'
    # Log into the calendar with the passed username/password
    calendar_service.ProgrammaticLogin()

    for obsid,block in zip(obsdict.values(),obsdict.keys()):
        gid = obsid[0]
        uid = obsid[1]
        content = obsid[2]
        if content != None: content = content.replace('\t','     ')
        title = gid+' - '+uid
        print "Inserting event:",title,block[0],block[1].time()

        BPL_cal = calendars['%s.%s.%s' % (tel,enc,site)]
        query = gdata.calendar.service.CalendarEventQuery(BPL_cal,'private', 'full')
        query.start_min = block[0].strftime('%Y-%m-%dT%H:%M:%S.000Z')
        edate = block[0] + timedelta(days=1)
        edate = datetime(edate.year,edate.month,edate.day,0,0,0,0)
        query.start_max = edate.strftime('%Y-%m-%dT%H:%M:%S.000Z')
        # Set calender timezone to be UTC
        query.ctz = 'Etc/UTC'
        feed = calendar_service.CalendarQuery(query)

        block_start_str = block[0].strftime('%Y-%m-%dT%H:%M:%S.000Z')
        exists = False
        try:
            for i, an_event in enumerate(feed.entry):
                if len(an_event.when) == 0: continue
                     #print '\t%s. %s' % (i, an_event.title.text,)
                for a_when in an_event.when:
                         #print an_event.title.text.strip(), title.strip(), an_event.title.text.strip() == title.strip()
                    if (a_when.start_time == block_start_str) and (an_event.title.text.strip() == title.strip()):
                        print "%s already exists in the Google Cal." % title.strip(), a_when.start_time
                        exists = True
                        break
                    else:
                        exists = False
                if exists: break
        except UnboundLocalError:
            print "Feed is empty."
        
        if not exists:
            event = gdata.calendar.CalendarEventEntry()
            event.title = atom.Title(text=title)
            event.content = atom.Content(text=content)
            event.where.append(gdata.calendar.Where(value_string='%s' % site))

            start_time = block[0].strftime('%Y-%m-%dT%H:%M:%S.000Z')
            end_time = block[1].strftime('%Y-%m-%dT%H:%M:%S.000Z')
            event.when.append(gdata.calendar.When(start_time=start_time, end_time=end_time))
            if not dry:
                for i in range(4):
                    try:
                        #print "Inserting event:",title,block[0],block[1].time()
                        new_event = calendar_service.InsertEvent(event, feed.link[1].href)
                        print "Success!"
                        break
                    except gdata.service.RequestError:
                        print "Error uploading to Google Cal. Trying again..."
                        time.sleep(3)
                        calendar_service = gdata.calendar.service.CalendarService()
                        calendar_service.email = cal_user
                        calendar_service.password = cal_passwd
                        calendar_service.source = 'Google-Calendar_Python_Sample-1.0'
                        calendar_service.ProgrammaticLogin()
                        #query = gdata.calendar.service.CalendarEventQuery(BPL_cal,'private', 'full')
                        #feed = calendar_service.CalendarQuery(query)
            else:
                print "Block %s already exists in calendar." % title

    #Check for orphaned events in the Google Cal
    block_start_strings = [block[0].strftime('%Y-%m-%dT%H:%M:%S.000Z') for block in obsdict.keys()]
    try:
        for i, an_event in enumerate(feed.entry):
                if len(an_event.when) == 0: continue
                #print '\t%s. %s' % (i, an_event.title.text,)
                for a_when in an_event.when:
                    #print an_event.title.text.strip(), title.strip(), an_event.title.text.strip() == title.strip()
                    if a_when.start_time not in block_start_strings and datetime.strptime(a_when.start_time,'%Y-%m-%dT%H:%M:%S.000Z') > datetime.utcnow():
                        print block_start_strings
                        print "No POND entry for event: %s, %s --> %s" % (an_event.title.text.strip(), a_when.start_time, a_when.end_time)
                        calendar_service = gdata.calendar.service.CalendarService()
                        calendar_service.email = cal_user
                        calendar_service.password = cal_passwd
                        calendar_service.source = 'Google-Calendar_Python_Sample-1.0'
                        calendar_service.ProgrammaticLogin()
                        try:
                            status = calendar_service.DeleteEvent(an_event.GetEditLink().href)
                        except gdata.service.RequestError:
                            print "Error uploading to Google Cal. Trying 1 more time..."
                            time.sleep(3)
                            calendar_service = gdata.calendar.service.CalendarService()
                            calendar_service.email = cal_user
                            calendar_service.password = cal_passwd
                            calendar_service.source = 'Google-Calendar_Python_Sample-1.0'
                            calendar_service.ProgrammaticLogin()
                            
                            status = calendar_service.DeleteEvent(an_event.GetEditLink().href)
    except UnboundLocalError:
        print "Feed is empty."

def submitObs(target,block,site='bpl',tel='1m0a',priority=10,dry=False):
    #print tel
    if tel == '1m0a' and site=='bpl':
        observatory = 'doma'
        inst = 'kb72'
    if tel == '0m4a' and site=='bpl':
        print "HERE?"
        observatory = 'aqwa'
        inst = 'kb89'
    if tel == '1m0a' and site=='elp':
        observatory = 'doma'
        inst = 'kb76'

    start = block[0]
    end = block[1]
    name = target['NAME']
    target_ra = target['RA']
    target_dec = target['DEC']
    filt = target['FILTER']
    if filt in filtdef.keys(): filt = filtdef[filt]
    exp = target['EXPTIME']
    binning = target['BINNING']
    cnt  = np.floor(((end - start).seconds - 300.0) / (exp + 15.0))      #number of exposures

    if str(target['RA']).find(':') != -1:
        ra_sex = [float(r) for r in target['RA'].split(':')]
        dec_sex = [float(d) for d in target['DEC'].split(':')]
        ra_deg = ra_sex[0]*15.0 + ra_sex[1]/60.0 + ra_sex[2]/3600.0
        ra_hr = ra_sex[0] + ra_sex[1]/60.0 + ra_sex[2]/3600.0
        dec_deg = dec_sex[0] + dec_sex[1]/60. + dec_sex[2]/3600.
        target['RA'] = target_ra = ra_hr
        target['DEC'] = target_dec = dec_deg
    
    target_ra = float(target_ra)
    target_dec = float(target_dec)

    print "Scheduling observations to %s.%s:" % (site,tel)
    print start,'-->', end
    print "   %s:\t%s" % ('NAME',name)
    print "   %s:\t\t%s" % ('RA',target['RA'])
    print "   %s:\t\t%s" % ('DEC',target['DEC'])
    print "   %s:\t%s" % ('EXPOSURE',exp)
    print "   %s:\t%s" % ('COUNT',int(cnt))
    print "   %s:\t%s" % ('FILTER',filt)
    print "   %s:\t%s" % ('BINNING',int(binning))
    print
    
    block = pond_client.ScheduledBlock(start, end, site, observatory, tel, priority)
    if not dry: block.save()

    group = pond_client.Group('LCO', target['owner'], target['proposal'], name)
    CSYS = 2       # 0=FK4, 1=FK5, 2=ICRS, 3=APPARENT
    
    point_params = {'source_name':name,'ra':float(target_ra),'dec':float(target_dec),'coord':CSYS}
    pondtarget = pond_client.Pointing.sidereal(**point_params)
    expose_params = {'len':exp,'cnt':cnt,'bin':binning,'inst':inst,'filter':filt,'target':pondtarget,'ag_mode':0}
    expose = group.add_expose(**expose_params)
    block.add_obs(expose, priority)

    if not dry: block.save()

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Insert LCOGT.net observations into the POND, and sync with the GoogleCals.',add_help=True)
    parser.add_argument('-d',dest='sdate',action='store',help='UT date of start of search window (YYYYMMDD)',default=datetime.strftime(datetime.utcnow(), format='%Y%m%d'),type=str)
    parser.add_argument('-n',dest='dry',action='store_true',help='Dry run. Print results, but do not upload to Google Cal.',default=False)
    parser.add_argument('--sync',dest='sync',action='store_true',help='Sync with GoogleCal only.',default=False)
    parser.add_argument('--manual',dest='man',action='store_true',help='Manually enter observations.',default=False)
    parser.add_argument('--tel',dest='tel',action='store',help='Telescope',default='1m0a')
    parser.add_argument('--site',dest='site',action='store',help='Site',default='bpl')
    opt = parser.parse_args()

    sdate = datetime.strptime(opt.sdate,'%Y%m%d')

    if opt.sync:
        start = sdate
        while sdate < start + timedelta(days=30):
            events,obsdict = getEvents(sdate,tel=opt.tel,site=opt.site)
            try:
                GoogleCalUpload(obsdict,dry=opt.dry,tel=opt.tel,site=opt.site)
            except UnboundLocalError:
                pass
            sdate += timedelta(days=1)

    elif opt.manual:
        start = datetime(2012,3,3, 3,16,0)
        end = datetime(2012,3,3, 6,58,0)
        name = 'CSS06571'
        target_ra = 7.65991  # coords of SA110
        target_dec = 56.70935
        filt = 'Johnson-B'   #filter, dashes
        exp  = 120.0    #exposure in sec
        binning  = 2.     #binning
        dry = opt.dry

        block = (start,end)
        target = {'NAME':name,'RA':target_ra, 'DEC':target_dec, 'FILTER':filt, 'EXPTIME':exp, 'BINNING':binning}
        submitObs(target,block,site=opt.site,tel=opt.tel)


